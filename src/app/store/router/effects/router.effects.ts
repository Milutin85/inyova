import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { createEffect, Actions, ofType } from '@ngrx/effects';

import * as routerActions from '../actions';

import { tap, map } from 'rxjs/operators';

@Injectable()
export class RouterEffects {
  constructor(
    private actions$: Actions,
    private router: Router,
    private location: Location
  ) {
  }

  navigate$ = createEffect(
    () => this.actions$.pipe(
      ofType(routerActions.Go),
      tap(({type, path, query: queryParams, extras}) => {
        this.router.navigate(path, {queryParams, ...extras});
      })
    ),
    { dispatch: false }
  );

  navigateBack$ = createEffect(
    () => this.actions$.pipe(
      ofType(routerActions.Back),
      tap(() => {
        this.location.back();
      })
    ),
    { dispatch: false }
  );

  navigateForward$ = createEffect(
    () => this.actions$.pipe(
      ofType(routerActions.Forward),
      tap(() => {
        this.location.forward();
      })
    ),
    { dispatch: false }
  );

}


