import { createReducer, on } from '@ngrx/store';

import * as fromInyovaActions from '../actions';

import { InyovaAccount } from '../../../models/inyova-account.interface';

export interface InyovaDetailsState {
  accountDetails: InyovaAccount | null;
  loading: boolean;
  loaded: boolean;
}

export const initialState: InyovaDetailsState = {
  accountDetails: null,
  loading: false,
  loaded: false
}

export const reducer = createReducer(
  initialState,
  on(fromInyovaActions.loadAccountDetails, (state) => ({
    ...state,
    loading: true,
    loaded: false
  })),
  on(fromInyovaActions.loadAccountDetailsSuccess, (state, {accountDetails}: {accountDetails: InyovaAccount | null}) => {

    return {
      ...state,
      ...(accountDetails !== null ? {
        accountDetails: {
          ...accountDetails,
          birthdate: {
            ...accountDetails.birthdate
          }
        }
      } : {}),
      loading: false,
      loaded: true
    }
  }),
  on(fromInyovaActions.loadAccountDetailsFailed, (state, {error}: {error: any}) => ({
    ...state,
    loading: false,
    loaded: false
  }))
)

export const getAccountDetails = (state: InyovaDetailsState) => state.accountDetails;
export const getLoading = (state: InyovaDetailsState) => state.loading;
export const getLoaded = (state: InyovaDetailsState) => state.loaded;
