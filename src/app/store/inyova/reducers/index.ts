import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import * as fromInyovaDetails from './inyova-details.reducer';

export interface InyovaState {
  account: fromInyovaDetails.InyovaDetailsState
}

export const reducers: ActionReducerMap<InyovaState> = {
  account: fromInyovaDetails.reducer
};

export const getInyovaState = createFeatureSelector<InyovaState>('inyova');
