import { createAction, props } from '@ngrx/store';
import { InyovaAccount } from '../../../models/inyova-account.interface';

// main
export const emptyAction = createAction(
  '[Inyova] No Action'
);
export const clearAction = createAction(
  '[Inyova] Clear'
);

// load
export const loadAccountDetails = createAction(
  '[Inyova Account] Load Details'
);
export const loadAccountDetailsSuccess = createAction(
  '[Inyova Account] Load Details Success', props<{accountDetails: InyovaAccount | null}>()
);
export const loadAccountDetailsFailed= createAction(
  '[Inyova Account] Load Details Failed', props<{error: any}>()
);

// save
export const saveAccountDetails = createAction(
  '[Inyova Account] Save Details', props<{accountDetails: InyovaAccount}>()
);
export const saveAccountDetailsSuccess = createAction(
  '[Inyova Account] Save Details Success'
);
export const saveAccountDetailsFailed= createAction(
  '[Inyova Account] Save Details Failed', props<{error: any}>()
);
