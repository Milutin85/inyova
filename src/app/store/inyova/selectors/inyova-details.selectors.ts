import { createSelector } from "@ngrx/store";

import * as fromReducer from '../reducers';
import * as fromDetailsReducer from '../reducers/inyova-details.reducer';

export const getInyovaAccountState = createSelector(
  fromReducer.getInyovaState,
  (state: fromReducer.InyovaState) => state.account
);

export const getInyovaAccountDetails = createSelector(
  getInyovaAccountState,
  fromDetailsReducer.getAccountDetails
);

export const getInyovaAccountLoading = createSelector(
  getInyovaAccountState,
  fromDetailsReducer.getLoading
);

export const getInyovaAccountLoaded = createSelector(
  getInyovaAccountState,
  fromDetailsReducer.getLoaded
);
