import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';

import { Store } from '@ngrx/store';
import { TypedAction } from '@ngrx/store/src/models';

import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { InyovaService } from '../../../services/inyova.service';
import { ToastrService } from "ngx-toastr";

import * as fromInyovaActions from '../actions';
import * as fromRouter from '../../router';

import { InyovaAccount } from '../../../models/inyova-account.interface';

@Injectable()
export class InyovaDetailsEffects {
  constructor(
    private actions$: Actions,
    private inyovaService: InyovaService,
    private store: Store,
    private toastrService: ToastrService
  ) {

  }

  loadAccountDetails$ = createEffect( () =>
    this.actions$.pipe(
      ofType(fromInyovaActions.loadAccountDetails),
      switchMap(() => {
        return this.inyovaService.loadAccountDetails().pipe(
          map((accountDetails: null | InyovaAccount) => {
            return fromInyovaActions.loadAccountDetailsSuccess({accountDetails});
          }),
          catchError(error => {
            return of(fromInyovaActions.loadAccountDetailsFailed({error}));
          })
        )
      })
    ));

  saveAccountDetails$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromInyovaActions.saveAccountDetails),
      switchMap(({accountDetails}: {accountDetails: InyovaAccount}) => {
        return this.inyovaService.saveAccountDetails(accountDetails).pipe(
          map(() => {
            return fromInyovaActions.saveAccountDetailsSuccess();
          }),
          catchError(error => {
            return of(fromInyovaActions.saveAccountDetailsFailed({error}));
          })
        )
      })
    ));

  saveAccountDetailsSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromInyovaActions.saveAccountDetailsSuccess),
      switchMap(() => {
        this.toastrService.success('Account details successfully saved', '', {
          timeOut: 2500,
          easeTime: 200,
          extendedTimeOut: 300
        });
        return of(fromRouter.Go({path: ['strategy']}));
      })
    )
  )


}
