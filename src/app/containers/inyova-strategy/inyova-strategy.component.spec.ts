import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InyovaStrategyComponent } from './inyova-strategy.component';

describe('InyovaStrategyComponent', () => {
  let component: InyovaStrategyComponent;
  let fixture: ComponentFixture<InyovaStrategyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InyovaStrategyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InyovaStrategyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
