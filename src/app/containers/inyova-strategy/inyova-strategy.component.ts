import { Component, OnInit } from '@angular/core';

import { Store } from "@ngrx/store";

import * as fromRouter from '../../store/router';

@Component({
  selector: 'app-inyova-strategy',
  templateUrl: './inyova-strategy.component.html',
  styleUrls: ['./inyova-strategy.component.scss']
})
export class InyovaStrategyComponent implements OnInit {

  constructor(
    private store: Store
  ) { }

  ngOnInit(): void {
  }

  onAccountOpen() {
    this.store.dispatch(fromRouter.Go({path: ['details']}));
  }
}
