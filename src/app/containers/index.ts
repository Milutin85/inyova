import { InyovaStrategyComponent } from './inyova-strategy/inyova-strategy.component';
import { InyovaDetailsComponent } from './inyova-details/inyova-details.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

export const components: any[] = [
  InyovaStrategyComponent,
  InyovaDetailsComponent,
  PageNotFoundComponent
];

export * from './inyova-strategy/inyova-strategy.component';
export * from './inyova-details/inyova-details.component';
export * from './page-not-found/page-not-found.component';
