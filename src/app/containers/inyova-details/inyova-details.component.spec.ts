import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InyovaDetailsComponent } from './inyova-details.component';

describe('InyovaDetailsComponent', () => {
  let component: InyovaDetailsComponent;
  let fixture: ComponentFixture<InyovaDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InyovaDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InyovaDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
