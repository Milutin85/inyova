import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormGroupDirective } from "@angular/forms";

import { Observable } from "rxjs";
import { take } from "rxjs/operators";

import { Store } from "@ngrx/store";
import { ToastrService } from "ngx-toastr";

import { InyovaAccount, InyovaAccountGender } from "../../models/inyova-account.interface";
import * as fromInyovaStore from '../../store/inyova';


@Component({
  selector: 'app-inyova-details',
  templateUrl: './inyova-details.component.html',
  styleUrls: ['./inyova-details.component.scss']
})
export class InyovaDetailsComponent implements OnInit {

  accountDetailsForm: FormGroup;
  inyovaAccountGender = InyovaAccountGender
  accountDetailsLoading$: Observable<boolean>;
  accountDetailsLoaded$: Observable<boolean>;

  get genderControl(): AbstractControl | null {
    return this.accountDetailsForm.get('gender');
  }

  get firstNameControl(): AbstractControl | null {
    return this.accountDetailsForm.get('firstName');
  }

  get lastNameControl(): AbstractControl | null {
    return this.accountDetailsForm.get('lastName');
  }

  get birthdateControl(): FormGroup {
    return this.accountDetailsForm.get('birthdate') as FormGroup;
  }

  get nationalityControl(): AbstractControl | null {
    return this.accountDetailsForm.get('nationality');
  }

  constructor(
    private store: Store<fromInyovaStore.InyovaState>,
    private fb: FormBuilder,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    // initialize form
    this.accountDetailsForm = this.initializeForm();
    // loading - loaded
    this.accountDetailsLoading$ = this.store.select(fromInyovaStore.getInyovaAccountLoading);
    this.accountDetailsLoading$ = this.store.select(fromInyovaStore.getInyovaAccountLoaded);

    // load details
    this.store.dispatch(fromInyovaStore.loadAccountDetails());

    this.store.select(fromInyovaStore.getInyovaAccountDetails)
      .pipe(take(1))
      .subscribe((accountDetails: InyovaAccount | null) => {

        if (accountDetails !== null) {
          this.setFormData(accountDetails);
        }
      });

  }

  initializeForm(): FormGroup {
    return this.fb.group({
      gender: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      birthdate: this.fb.group({
        day: ['', [Validators.required]],
        month: ['', [Validators.required]],
        year: ['', [Validators.required]]
      }),
      nationality: ['', [Validators.required]]
    });
  }

  setFormData(accountDetails: InyovaAccount): void {
    this.accountDetailsForm?.patchValue(accountDetails);
    this.accountDetailsForm?.updateValueAndValidity();
  }

  onSubmit(event: any) {
    if (this.accountDetailsForm.invalid) {
      this.toastrService.error('Some fields are missing', '', {
        positionClass: 'toast-bottom-right',
        timeOut: 2500,
        easeTime: 200,
        extendedTimeOut: 300
      });

      return;
    }

    const accountDetails = this.accountDetailsForm.value;

    this.store.dispatch(fromInyovaStore.saveAccountDetails({accountDetails}));
  }



}
