import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InyovaHeaderComponent } from './inyova-header.component';

describe('InyovaHeaderComponent', () => {
  let component: InyovaHeaderComponent;
  let fixture: ComponentFixture<InyovaHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InyovaHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InyovaHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
