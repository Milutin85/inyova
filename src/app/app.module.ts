import { environment } from '../environments/environment';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { ToastrModule } from "ngx-toastr";

// store
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { RouterStateSerializer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { reducers, effects } from './store/inyova';

import * as fromRouter from './store/router';
import * as fromInyovaActions from './store/inyova/actions';

// services
import { InyovaService } from './services/inyova.service';

// import components
import { AppComponent } from './app.component';
import * as fromContainers from './containers';
import * as fromComponents from './components';



export function clearState(reducer: any) {
  return (state: any, action: any) => {
    if (action.type === fromInyovaActions.clearAction.type) {
      state = undefined;
    }
    return reducer(state, action);
  }
}

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/strategy'
  },
  {
    path: 'strategy',
    component: fromContainers.InyovaStrategyComponent
  },
  {
    path: 'details',
    component: fromContainers.InyovaDetailsComponent
  },
  {
    path: '**',
    component: fromContainers.PageNotFoundComponent
  }
  ];

@NgModule({
  declarations: [
    AppComponent,
    ...fromContainers.components,
    ...fromComponents.components
  ],
  imports: [
    BrowserModule,
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    StoreRouterConnectingModule.forRoot(),
    StoreModule.forRoot(fromRouter.reducers),
    EffectsModule.forRoot(fromRouter.effects),
    StoreModule.forFeature('inyova', reducers, { metaReducers: [ clearState] }),
    EffectsModule.forFeature(effects),
    environment.production ? [] : StoreDevtoolsModule.instrument()
  ],
  providers: [
    InyovaService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
