import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { InyovaAccount } from '../models/inyova-account.interface';

function getLocalStorage(): Storage {
  return localStorage;
}

@Injectable()
export class InyovaService {

  get localStorage(): Storage {
    return getLocalStorage();
  }

  constructor() {
  }

  loadAccountDetails(): Observable<InyovaAccount | null> {
    const jsonData: string | null = this.localStorage.getItem('accountDetails');
    const accountDetails = jsonData === null ? null : JSON.parse(jsonData);
    return of(accountDetails);
  }

  saveAccountDetails(accountDetails: InyovaAccount): Observable<string> {
    const jsonData = JSON.stringify(accountDetails);
    this.localStorage.setItem('accountDetails', jsonData);
    return of(jsonData);
  }

}
