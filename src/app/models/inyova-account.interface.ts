export enum InyovaAccountGender {
  MALE = 'MALE',
  FEMALE = 'FEMALE'
}

export interface InyovaAccount {
  gender: InyovaAccountGender;
  firstName: string;
  lastName: string;
  birthdate: {
    day: string;
    month: string;
    year: string;
  }
  nationality: string;
}
